delete from `users` where `login` = 'admin';

insert into `users` (
    `login`,
    `hashed_password`,
    `firstname`,
    `lastname`,
    `admin`,
    `status`,
    `type`,
    `mail_notification`,
    `salt`
) values (
    'admin',
    '69c752b9bee79fe69cc58debbfdcd40150ba113a',
    'Redmine',
    'Admin',
    't',
    '1',
    'User',
    'all',
    '7cb24464f494785b15bd26e0eb950bcc'
);
