# Atualizacao do Redmine

Fala seus puto, beleza?? Este eh o _playbook_ pra atualizar o redmine atual
(1.2.1) pra versao 3.0.3.

## Como usar:

### 1. Instalar [Ansible](http://docs.ansible.com/intro_installation.html)

Versao 1.7.2 ou mais recente, e confere se ele esta no seu `$PATH`:

    $ which ansible

### 2. Baixar esse playbook

via [Git](https://git-scm.com/):

    $ git clone https://bitbucket.org/TIGE/upgrade_redmine.git

ou soh faz o [download](https://bitbucket.org/TIGE/upgrade_redmine/get/master.tar.bz2)
memo ...

### 3. Configurar acesso a maquina que hospeda o redmine

Bota as credenciais no arquivo `hosts.staging`:

    [redmine]
    tb2.tige.com.br

    [redmine:vars]
    ansible_ssh_port=7270
    ansible_ssh_user=sysop
    ansible_ssh_pass=XXXXXXX
    ansible_sudo_pass=XXXXXXX

#### Notas:

Confere se sua maquina consegue acessar o servidor, soh pra garantir:

    $ ssh -p 7270 sysop@tb2.tige.com.br

O usuario `sysop` (ou qqr um que vc queira usar) precisa ter permissao pra
usar `sudo`. Caso ele nao tenha, vc pode fazer o seguinte:

    $ ssh -p 7270 sysop@tb2.tige.com.br
    
    sysop@ws5:~$ su
    Password: <senha de root>
    
    root@ws5:# echo "sysop ALL=(ALL) ALL" >> /etc/sudoers

Eu reparei que o usuario `sysop` nao tinha permissao de escrita no proprio
diretorio `$HOME` dele (`/home/sysop`) ... nao tem problema ser um symlink pro
`/disk2/qqrcoisa/`, mas eh importante que ele possa escrever arquivos:

    root@ws5:# chown -R sysop. /home/sysop /home/sysop/* /home/sysop/.*

**MUITO IMPORTANTE:** soh pq o autor deste playbook eh foda pra caralho,
pirocudo e humilde, nao significa que vc pode confiar 100% e rodar isso direto
em producao.
Testa antes, cria um clone (assim como esse tb2) e roda lah primeiro.

### 4. Roda a bagassa !!!

Simprao:

    $ cd /path/to/upgrading_redmine
    $ ansible-playbook -i hosts.staging upgrading_redmine_for_tige.yml

Isso deve demorar mais ou menos 35 minutos, se der tudo certo vc vai ver uma
linha assim no final do output:

    PLAY RECAP ****************************************************************
    tb3.tige.com.br            : ok=X    changed=X    unreachable=0    failed=0

Se der merda vc vai ver um monte de erro vermelho tudo fudido que vai te
desesperar muito ... nesse caso copia o erro todo e me manda por email ;D

### Plugins:

Nao deu pra aproveitar os plugins, a maioria foi abandonada e nao eh
compativel com a versao 3.x do redmine ... mas na conversa com o nosso querido
Ey Ey Eymanuel, um democrata cristao! os mais importantes eram SCM e
CheckList, entao tentei deixar eles funfando, mas preciso que vcs testem
extensivamente, pq as coisas mudaram muito da versao 1 pra 3 do redmine.
